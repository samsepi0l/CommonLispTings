(setq *print-case* :downcase)

(defvar *username* nil)
(defvar *password* nil)
(defvar *new-username* nil)
(defvar *new-password* nil)

(defparameter *accounts* nil)

(defvar inc 0)


(loop (setq cm (read))
      (if (eq cm 'q)
          (return))

      (if (eq cm 'create)
          (progn
            (format t "New Username: ")
            (setq *new-username* (read))
            (format t "New password: ")
            (setq *new-password* (read))
            (push (list :username *new-username* :password *new-password*) *accounts*)
          ))

      (if (eq cm 'login)
          (progn
            (format t "Username: ")
            (setq *username* (read))
            (format t "Password: ")
            (setq *password* (read))
            (dolist (acc *accounts*)
              (defparameter check1 (getf (nth inc *accounts*) :username))
              (defparameter check2 (getf (nth inc *accounts*) :password))
              (if (and (equalp *username* check1) (equalp *password* check2))
                  (progn
                    (format t "Thank you for logging in, ~a~%" *username*)
                    (format t "~a is now ready to play!~%" *username*))
                  (format t "please type <create> to create an account. Otherwise, enter some valid information."))))))
