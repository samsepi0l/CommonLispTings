(setq *print-case* :downcase)

(format t "Username: ")
(defvar *username* (read))
(format t "Password: ")
(defvar *password* (read))


(defparameter *accounts* nil)

(defvar admin1 (list :username 'sweatshirt :password 'Daemons0!))
(defvar user1 (list :username 'samsepi0l :password 'Daemons))
(defvar user2 (list :username 'mogad0n :password 'something))

(push admin1 *accounts*)
(push user1 *accounts*)
(push user2 *accounts*)

(defparameter varify 'false)
(defparameter inc 0)

(dolist (acc *accounts*)
  (defparameter check1 (getf (nth inc *accounts*) :username))
  (defparameter check2 (getf (nth inc *accounts*) :password))
  (if (and (equalp *username* check1) (equalp *password* check2))
      (setq varify 'true)
      (progn
        (setq inc (+ inc 1)))))

(if (equalp varify 'true)
    (progn
      (format t "Thank you so much for logging in, ~a~%" *username*))
    (format t "try again~%"))
