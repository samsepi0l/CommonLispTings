(setq *print-case* :downcase)

(format t "Whats your name? ~%")

(defvar *name* (read))

(defun greetings (name)
  (format t "It's nice to meet you, ~a, I'm emacs. ~%" name)
  (format t "How are you, ~a? ~%" name)
  (defparameter good 'good)
  (defvar how (read))
  (if (eq how good)
      (progn
        (format t "I'm glad to hear you are ~a! ~%" how)
        (format t "Have a great rest of your day, ~a!" name))
      (format t "just because your day was ~a now doesn't mean it will be ~a forever! ~%" how how)))

(greetings *name*)
