(defparameter *server* (make-instance 'tbnl:easy-acceptor
                                      :port 33333
                                      :address "0.0.0.0"))
(defun start ()
  (tbnl:start *server*))

(defun stop()
  (tbnl:stop *server*))

(defparameter *css*
  "
body {
background: #f0f2f5;
font-family: monospace;
}

.nav-wrapper {
background-color: blue;
padding: 15px;
}

.form-wrapper {
position: relative;
top: 55px;
padding: 15px 10px;
background-color: #fff;
width: 400px;
border-radius: 5px;
box-shadow: 8px 8px 8px lightgrey;
}

.username {
position: relative;
display: flex;
margin-left: auto;
margin-right: auto;
border: 1px solid lightgrey;
border-radius: 5px;
padding: 15px;
font-size: 1.2rem;
width: 95%;
}

.password {
position: relative;
display: flex;
margin-left: auto;
margin-right: auto;
border: 1px solid lightgrey;
border-radius: 5px;
padding: 15px;
font-size: 1.25rem;
width: 95%;
}

.nav-wrapper a {
margin-left: 15px;
margin-right: 15px;
text-decoration: none;
font-size: 1rem;
color: #fff;
}

.nav-wrapper a:hover {
cursor: pointer;
text-decoration: underline;
}

.title-wrapper {
display: flex;
justify-content: center;
}

.content {
font-size: 1.1rem;
}

.contact-content-subtitle {
font-size: 1.1rem;
}

.contact-content-irc {
font-size: 1.1rem;
}

.contact-content-xmpp {
font-size: 1.1rem;
}

.login-button {
position: relative;
display: flex;
margin-left: auto;
margin-right: auto;
width: 90%;
padding: 12px;
background-color: blue;
color: #fff;
border: 1px solid lightgrey;
border-radius: 5px;
font-size: 1.0rem;
font-weight: bold;
}

.login-button:hover {
cursor: pointer;
box-shadow: 8px 8px 8px lightgrey;
}

.signup-button {
position: relative;
display: flex;
margin-left: auto;
margin-right: auto;
background-color: #4af626;
width: 90%;
border: 1px solid lightgrey;
border-radius: 5px;
}

.signup-link {
position: relative;
display: flex;
justify-content: center;
padding: 12px;
width: 100%;
font-size: 1rem;
font-weight: bold;
color: black;
text-decoration: none;
}

.signup-button:hover {
cursor: pointer;
box-shadow: 8px 8px 8px lightgrey;
}

.landing-content-wrapper {
position: relative;
left: 47vw;
top: -30vh;
background-color: #fff;
width: 50%;
padding: 10px 5px;
border: 1px solid lightgrey;
border-radius: 5px;
box-shadow: 8px 8px 8px lightgrey;
}

.landing-content-title {
text-align: center;
font-size: 1.1rem;
}

.landing-content {
text-align: center;
font-size: 1rem;
}")

(tbnl:define-easy-handler (main :uri "/")
  ()
  (spinneret:with-html-string
    (:html
     (:head
      (:style
        *css*))
     (:body
      (:div :class "whole-wrapper"
            (:div :class "nav-wrapper"
                  (:a :class "home-link" :href "/"
                      "Home")
                  (:a :class "about-link" :href "/about"
                      "About")
                  (:a :class "contact-link" :href "/contact"
                      "Contact"))
            (:div :class "title-wrapper"
                  (:h1 :class "title"
                       "Welcome to my CL server!"))
            (:div :class "form-wrapper"
                  (:form :class "login" :action "/members" :method "POST"
                         (:input :type "text" :class "username" :name "username" :placeholder "Username")
                         (:br)(:br)
                         (:input :type "password" :class "password" :name "password" :placeholder "Password")
                         (:br) (:br)
                         (:input :type "submit" :class "login-button" :value "login")
                         (:br) (:br)
                         (:button :class "signup-button"
                                  (:a :class "signup-link" :href "/signup"
                                      "Sign up"))))
            (:div :class "landing-content-wrapper"
                  (:h3 :class "landing-content-title"
                  "----UPDATES----")
                  (:p :class "landing-content"
                      "Still undergoing development. Thank you for your patience!")))))))

(tbnl:define-easy-handler (about :uri "/about")
  ()
  (spinneret:with-html-string
    (:html
     (:head
      (:style
       *css*))
     (:body
      (:div :class "whole-wrapper"
            (:div :class "nav-wrapper"
                  (:a :class "home-link" :href "/"
                      "Home")
                  (:a :class "about-link" :href "/about"
                      "About")
                  (:a :class "contact-link" :href "/contact"
                      "Contact"))
            (:div :class "content-wrapper"
                  (:div :class "about-title-wrapper"
                        (:h1 "About this site"))
                  (:div :class "content-wrapper"
                        (:p :class "content"
                            "I am an intermediate programmer who is new-ish to common lisp
                             and am loving it so far! So much so that I have decided to practice
                             making a webserver; though i usually do this with node js, handling
                             request with response, i decided to do the same with common lisp!
                             I will be using hunchentoot, spinneret and lass(maybe?) to create
                             this site; I hope you enjoy :)"))))))))

(tbnl:define-easy-handler (contact :uri "/contact")
  ()
  (spinneret:with-html-string
    (:head
     (:style
      *css*))
    (:body
     (:div :class "whole-wrapper"
           (:div :class "nav-wrapper"
                 (:a :class "home-link" :href "/"
                     "Home")
                 (:a :class "about-link" :href "/about"
                     "About")
                 (:a :class "contact-link" :href "/contact"
                     "Contact"))
           (:div :class "content-wrapper"
                 (:h1 :class "contact-content-title"
                      "Contact")
                 (:h3 :class "contact-content-subtitle"
                      "IRC")
                 (:p :class "contact-content-irc"
                     "sweatshirt, samsepi0l")
                 (:h3 :class "contact-content-subtitle"
                      "XMPP")
                 (:p :class "contact-content-xmpp"
                     "sweatshirt@xmpp.jp"))))))

(tbnl:define-easy-handler (signup :uri "/signup")
  ()
  (spinneret:with-html-string
    (:head
     (:style
      *css*))
    (:body
     (:div :class "whole-wrapper"
           (:div :class "nav-wrapper"
                 (:a :class "home-link" :href "/"
                     "Home")
                 (:a :class "about-link" :href "/about"
                     "About")
                 (:a :class "contact-link" :href "/contact"
                     "Contact"))
           (:div :class "signup-content-wrapper"
                 (:h1 :class "signup-content-title"
                      "Sign Up"))))))
