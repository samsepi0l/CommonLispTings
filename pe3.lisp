(defvar div 2)
(defvar num 317584931803)

(loop
  (if (zerop(mod num div))
      (progn
        (setq num (/ num div))
        (setq div (- div 1)))
      (setq div (+ div 1)))
  (when (< num 1) (return div)))
