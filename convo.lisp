(setq *print-case* :downcase)

(format t "Hello there, what is your name? ~%")
(defvar *name* (read))

(defun greetings (name)
  (format t "Nice to meet you ~a, i'm emacs. ! ~%" name)
  (format t "How is your day? ~%")
  (defvar how (read))
  (defparameter good 'good)
  (defparameter bad 'bad)

  (if (eq how good)
      (progn
        (format t "I'm glad to hear that your day was ~a, ~a! ~%" how name)
        (format t "I hope you continue to have a ~a day! Till next time! ~%" how)))
      (if (eq how bad)
          (progn
            (format t "I'm sorry to hear that your day was ~a, ~a... ~%" how name)
            (format t "I hope your day is better next time, ~a. ~%" name)))
      (format t "That's interesting to hear, ~a. Let's aim for a good day next time! ~%" name))

(greetings *name*)
