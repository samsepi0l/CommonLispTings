(setq *print-case* :downcase)

(format t "Enter your username: ")
(defvar *username* (read))
(format t "Enter your password: ")
(defvar *password* (read))

(setq use 'sweatshirt)
(setq pass 'Daemons0!)

(defun varify (username password)
       (if (and (eq username use) (eq password pass))
           (progn
             (format t "Thank you for logging in, ~a! ~%" username)
             (format t "Will check again soon! ~%"))
           (format t "You are not authorized to use this device, ~a." username)))

(varify *username* *password*)
