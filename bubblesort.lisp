(setq *print-case* :downcase)

(defvar li (list 4 8 9 2 1))

(defvar array nil)

(push li array)

(defparameter inc 1)

(defvar next (nth inc li))

(dolist (el li)
  (if (> el next)
      (progn
        (defparameter tmp el)
        (setq el next)
        (setq next tmp)
        (setq inc (+ inc 1))
        (if (< (nth (- inc 2) li) el))
            (push el array))
      (progn
        (setq inc (+ inc 1))
        (push next array)))
    (format t "~a~%" array))


(format t "~a" li)
