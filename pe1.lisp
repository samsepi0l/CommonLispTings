
(defvar *sum* 0)

(loop for x from 1 to 999 do
      (if (or (zerop(mod x 3)) (zerop (mod x 5)))
          (progn
            (setq *sum* (+ *sum* x)))))
(format t "~d ~%" *sum*)
